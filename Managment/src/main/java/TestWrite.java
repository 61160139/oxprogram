import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestWrite {
public static void main(String[] args) {
        ArrayList<User> list = new ArrayList();
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        User user1 = new User("user1", "password");
        User user2 = new User("user2", "password");
        list.add(user1);
        list.add(user2);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWrite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWrite.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos != null) {
                    oos.close();
                }
                if (fos != null) {
                    fos.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(TestWrite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
