
public class Table {
    private  char[][] tableOX = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    private  Player first;
    private  Player second;
    private  Player O;
    private  Player X;
    private  Player winner;
    private  Player player;
    private  int countturn = 0;
    
    public Table(Player first, Player second) {
        this.first = first;
        this.second = second;
        this.player = first;
        this.O = first ;
        this.X = second ;
    }
    
    public  Player getPlayer() {
        return player;
    }
    
     public void setTable(int row, int col) {
        tableOX[row - 1][col - 1] = player.getName();
        checkWin();
        if (checkWin() == false) {
            switchPlayer();
        }
    }
     
         public  void switchPlayer() {
        if (player == first) {
            player = second;
        } else if (player == second) {
             player = first;
        }
    }
         
          public  boolean checkWin() {
        if (checkWinRow()) {
            winner = player;
            return true;
        } else if (checkWinCol()) {
            winner = player;
            return true;
        } else if (checkWinX()) {
            winner = player;
            return true;
        }
        return false;
    }   
      public  void printTable() {
        for (int i = 0; i < tableOX.length; i++) {
            for (int j = 0; j < tableOX.length; j++) {
                System.out.print(tableOX[i][j] + " ");
            }
            System.out.println();
        }
    }    
      
    public  boolean checkWinRow() {
        if (tableOX[0][0] == player.getName() && tableOX[0][1] == player.getName()
                && tableOX[0][2] == player.getName()) {
            return true;
        } else if (tableOX[1][0] == player.getName() && tableOX[1][1] == player.getName()
                && tableOX[1][2] == player.getName()) {
            return true;
        } else if (tableOX[2][0] == player.getName() && tableOX[2][1] == player.getName()
                && tableOX[2][2] == player.getName()) {
            return true;
        }

        return false;
    } 
    
    public  boolean checkWinCol() {
        if (tableOX[0][0] == player.getName() && tableOX[1][0] == player.getName()
                && tableOX[2][0] == player.getName()) {
            return true;
        } else if (tableOX[0][1] == player.getName() && tableOX[1][1] == player.getName()
                && tableOX[2][1] == player.getName()) {
            return true;
        } else if (tableOX[0][2] == player.getName() && tableOX[1][2] == player.getName()
                && tableOX[2][2] == player.getName()) {
            return true;
        }

        return false;
    }
    
    public  boolean checkDraw() {
        if (countturn == 9 && checkWin() == false) {
            return true;
        }
        return false;
    }
    
    public  boolean checkWinX1() {
        for (int i = 0; i < tableOX.length; i++) {
            if (tableOX[i][i] != player.getName()) {
                return false;
            }
        }
        return true;
    }
    
        public  boolean checkWinX2() {
        for (int i = 0; i < tableOX.length; i++) {
            if (tableOX[i][2 - i] != player.getName()) {
                return false;
            }
        }
        return true;
    }
    
    
    public  boolean checkWinX() {
        if (checkWinX1()) {
            return true;
        } else if (checkWinX2()) {
            return true;
        }
        return false;
    }
    
    
        public boolean checkError(int row, int col) {
        if (row > 3 || row < 1 || col > 3 || col < 1) {
            System.out.println("Error Row : Please input 1, 2, 3");
            return true;
        } else if (tableOX[row - 1][col - 1] == 'O'
                || tableOX[row - 1][col - 1] == 'X') {
            System.out.println("You can't select this location "
                    + "because it has already been chosen.");
            return true;
        }
        return false;
    }
        
    public  void scoreUpdate() {
        if (winner.getName() == 'O') {
            O.win();
            X.lose();
        } else if (winner.getName() == 'X') {
            X.win();
            O.lose();
        } else if (checkDraw()) {
            X.draw();
            O.draw();
        }
    }  
    public Player getWinner() {
        return winner;
    }
    
}
