class Player {
    
    Table board;
    char whoWin;
    private int xWin;
    private int xLost;
    private int xDraw;
    private int oWin;
    private int oLost;
    private int oDraw;

    Player() {
        xWin = 0;
        oWin = 0;
        xLost = 0;
        oLost = 0;
        xDraw = 0;
        oDraw = 0;
    }
    
    void setScore() {
        char win = board.whoWin();

        if (win == 'O') {
            this.setoWin();
            this.setxLost();
        } else if (win == 'X') {
            this.setxWin();
            this.setoLost();
        } else if (win == 'd') {
            this.setDraw();
        } else {
            System.out.println("Something Error!");
        }
    }
   void setxWin() {
        this.xWin = (xWin + 1);
    }
   
   void setxLost() {
        this.xLost = (xLost + 1);
    }
   
       void setoWin() {
        this.oWin = (oWin + 1);
    }

    void setoLost() {
        this.oLost = (oLost + 1);
    }

    void setDraw() {
        this.xDraw = (xDraw + 1);
        this.oDraw = (oDraw + 1);
    }

    void printScore() {
        System.out.println("Player 0  Win : " + oWin + " Lost : " + oLost + " Draw : " + oDraw);
        System.out.println("Player X  Win : " + xWin + " Lost : " + xLost + " Draw : " + xDraw);

    }


}